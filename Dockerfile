FROM ubuntu::16.10

# Setup ubuntu
RUN apt-get update && apt-get install -y wget

# Create user
RUN adduser docker

# Make work dir
RUN mkdir -p /app
RUN chown docker:docker /app

# Change user
USER docker

# Git clone
WORKDIR /app
# ${USER_NAME}でリポジトリは指定したい
RUN git clone --depth 1 git@bitbucket.org:narabo/angular2_with_spine.git

# Setup each environment
# 必要に応じた処理を追加する
# 必要であればshを作成してコピーしてからコンテナ内で実行するのもありかもしれない